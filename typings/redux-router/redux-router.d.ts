// declare module "redux-router" {
//   export var reduxReactRouter
//   export var routerStateReducer
//   export var ReduxRouter
//   export var pushState
//   export var replaceState
// }

declare module "react-router-redux" {

  interface RouteActions {
    push(path: string): any
  }

  export const UPDATE_LOCATION: string

  export const syncHistory
  export const routeReducer
  export const routeActions: RouteActions
}

declare module "history/lib/createBrowserHistory" {
  export var createHistory
}

declare module "history/lib/createHashHistory" {
  export var createHistory
}

declare module "history/lib/createMemoryHistory" {
  export var createHistory
}
