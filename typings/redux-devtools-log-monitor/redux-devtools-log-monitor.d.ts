// Type definitions for redux-devtools-dock-monitor 1.0.1
// Project: https://github.com/gaearon/redux-devtools-dock-monitor
// Definitions by: Petryshyn Sergii <https://github.com/mc-petry>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference path="../react/react.d.ts" />

declare module "redux-devtools-log-monitor" {
  import * as React from 'react'

  export default class LogMonitor extends React.Component<any, any> {}
}
