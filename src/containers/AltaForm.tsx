
import * as React from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import { bindActionCreators } from 'redux'
const moment = require('moment')

import * as axios from 'axios'


// State selector
const mapStateToProps = createSelector(
  state => state.router,
  (router) => {return {router}}
)

@connect(mapStateToProps)
class AltaForm extends React.Component<any, any> {

  // Actions need to be a member of the clase
  // (Note that this is typescript specific)
  private actions

  constructor() {
    super()
    this.actions = null
    this.state = {
      loading: false,
      fechaInicio: moment(new Date).add(15, 'days')
    }
  }

  bindProp = (name) =>  e => {
    this.setState({[name]: e.target.value})
  }

  handleSubmit = async () => {
    this.setState({
      loading: true
    })

    const {data: processes} = await axios.get('/bonita/API/bpm/process?s=Alta')
    
    await axios.post(`/bonita/API/bpm/process/${processes[0].id}/instantiation`, {
      solicitudPoliza: this.state
    })

    window.location.href = "/bonita/portal/homepage#?_pf=1&_p=tasklistinguser"
  }

  render() {
    //  {modeloCoche,matricula,nombreConductor,apellidosConductor,importePresupuesto,fechaInicio}
    return (
      <div className="ui top attached segment" style={{padding: '3.5em 1em 1em'}}>
        <form ref="formulario" className="ui form">
          <h3 className="ui dividing header">Solicitud Poliza Coche</h3>
          <div className="field">
            <label>Coche</label>
            <div className="two fields">
              <div className="field">
                <input
                  type="text"
                  placeholder="Modelo coche"
                  onChange={this.bindProp('modeloCoche')}
                  value={this.state.modeloCoche}
                />
              </div>
              <div className="field">
                <input
                  type="text"
                  placeholder="Matricula"
                  onChange={this.bindProp('matricula')}
                  value={this.state.matricula}
                />
              </div>
            </div>
          </div>
          <div className="field">
            <label>Datos Conductor</label>
            <div className="two fields">
              <div className="field">
                <input
                  type="text"
                  placeholder="Nombre"
                  onChange={this.bindProp('nombreConductor')}
                  value={this.state.nombreConductor}
                />
              </div>
              <div className="field">
                <input
                  type="text"
                  placeholder="Apellidos"
                  onChange={this.bindProp('apellidosConductor')}
                  value={this.state.apellidosConductor} />
              </div>
            </div>
          </div>
          <div className="field">
            <label>Presupuesto</label>
            <div className="four wide field">
              <input
                type="number"
                placeholder="Importe"
                onChange={this.bindProp('importePresupuesto')}
                value={this.state.importePresupuesto} />
            </div>
          </div>
          <div className="field">
            <label>Fecha</label>
            <div className="four wide field">
              <input
                type="text"
                disabled
                placeholder="Inicio"
                onChange={this.bindProp('fechaInicio')}
                value={this.state.fechaInicio.format('YYYY-MM-DD')} />
            </div>
          </div>
          <div className="ui button" tabIndex={0} onClick={this.handleSubmit}>Aceptar</div>
        </form>
        {this.state.loading
          ?  <div className="ui segment">
                <p></p>
                <div className="ui active dimmer">
                  <div className="ui loader"></div>
                </div>
              </div>
          : null
        }
        <div className="ui top attached label">
          Introduccion de Datos
        </div>
      </div>
    )
  }
}

export default AltaForm
