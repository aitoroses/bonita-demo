
import * as React from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import { bindActionCreators } from 'redux'
const moment = require('moment')

import * as axios from 'axios'


// State selector
const mapStateToProps = createSelector(
  state => state.router,
  (router) => {return {router}}
)

@connect(mapStateToProps)
class Worklist extends React.Component<any, any> {

  // Actions need to be a member of the clase
  // (Note that this is typescript specific)
  private actions

  constructor() {
    super()
    this.actions = null
    this.state = {
      tasks: []
    }
  }

  async componentDidMount() {
    const {data: processes} = await axios.get('/bonita/API/bpm/process?s=Alta')
    const {data: tasks} = await axios.get(`/bonita/API/bpm/task?f=processId=${processes[0].id}`)
    this.setState({tasks: tasks})
  }

  renderRow(task) {
    return (
      <tr>
        <td className="collapsing">
          <i className="mail outline icon"></i>{task.name}
        </td>
        <td><a href="#">{task.caseId}</a></td>
        <td>{moment(task.dueDate).fromNow()}</td>
      </tr>
    )
  }

  render() {
    //  {modeloCoche,matricula,nombreConductor,apellidosConductor,importePresupuesto,fechaInicio}
    return (
      <div>
        <table className="ui celled striped table">
          <thead>
            <tr>
              <th>Tareas</th>
              <th>Case ID</th>
              <th>Fecha Inicio</th>
            </tr>
          </thead>
          <tbody>
            {this.state.tasks
              ? this.state.tasks.map(this.renderRow)
              : <tr><td colSpan={3}>No hay tareas disponibles</td></tr>
            }
          </tbody>
        </table>
        { !this.state.tasks.length
          ?  <div className="ui segment">
                <p></p>
                <div className="ui active dimmer">
                  <div className="ui loader"></div>
                </div>
              </div>
          : null
        }
      </div>
    )
  }
}

export default Worklist
