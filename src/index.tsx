//////////////////
// Dependencies //
//////////////////

import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Router, Route, hashHistory } from 'react-router'
import { Provider } from 'react-redux'

///////////////////////
// Components import //
///////////////////////

// -- Import components --
import AltaForm from './containers/AltaForm'
import Worklist from './containers/Worklist'

///////////
// Store //
///////////

import {storePromise} from './store/configureStore'

/*****************************************************************
 * Root component                                                *
 * Is the top level component, it wraps the application entirely *
 * for rerendering in case of changes in the Redux Store         *
 *                                                               *
 * Here we are defining the application routes.                  *
 *                                                               *
 * The MainHandler is the component that acts as layout for the  *
 * others, passed as children to it.                             *
 *****************************************************************/

class Root extends React.Component<any, any> {

  state = {
    store: null,
    DevTools: null
  }

  async componentDidMount() {
    let {store, DevTools} = await storePromise
    this.setState({
      store,
      DevTools
    })
  }

  render() {
    const {store, DevTools} = this.state

    if (!store) return <noscript />

    return (
      <Provider store={store}>
        <div>
        <Router key="" ref="" history={hashHistory}>
          {/* Routes */}
          <Route path='/AltaForm' component={ AltaForm } />
          <Route path='/Worklist' component={ Worklist } />
        </Router>
        { DevTools ? <DevTools store={store} /> : null }
        </div>
      </Provider>
    )
  }
}

///////////////////////////
// Application Bootstrap //
///////////////////////////

/***************************************************************************
 * The application render target is the root node in the index.html folder *
 *                                                                         *
 * In case that we are in development mode with DEBUG=true the redux state *
 * monitor will appear also.                                               *
 ***************************************************************************/
document.addEventListener('DOMContentLoaded', function() {
  const target = document.getElementById('root')
  //if (__DEV__) {
    ReactDOM.render(
        <Root />,
      target
    )
})
