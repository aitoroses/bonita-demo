export var storePromise
export var store
export var DevTools

const assignVars = ({store: s, DevTools: d}) => {
  store = s
  DevTools = d

  return arguments[0]
}

if ( localStorage.getItem('__DEVTOOLS_ENABLED__')) {
  storePromise = System.import('./configureStore.dev')
} else {
  storePromise = System.import('./configureStore.prod')
}

 storePromise.then(({store: s, DevTools: d}) => {store = s; DevTools = d})
