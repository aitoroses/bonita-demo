'use strict'

// let express = require('express')

let webpack = require('webpack')
let WebpackDevServer = require('webpack-dev-server')
let compression = require('compression')

var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

//
// -------- Start Config -------------------
let config = require('./webpack.config.base.js')

config.plugins.push(new webpack.HotModuleReplacementPlugin())
config.plugins.push(new webpack.NoErrorsPlugin())
config.plugins.push(new CommonsChunkPlugin({
  name: 'vendor',
  filename: 'vendor.js'
}))

config.output = {
  chunkFilename: '[id].chunk.js',
}
config.output.path = '/'
config.output.publicPath = 'lib/' // Needed by hot app updates path
config.devServer = {

  // HMR configuration
  host: process.env.HMR_HOST || "dev-server", // Used for client connection
  port: process.env.HMR_PORT || 8081,
  path: process.env.HMR_PATH || "",

  hot: true,
  quiet: false,
  noInfo: false,
  publicPath: `/${config.output.publicPath}`, // Where to serve assets from
  filename: "bundle.js",
  stats: {
    colors: true,
    hash: false,
    timings: false,
    assets: true,
    chunks: true,
    chunkModules: true,
    modules: false,
    children: true
  }
}

config.entry = {
  vendor: [
    `webpack-dev-server/client?http://${config.devServer.host}:${config.devServer.port}${config.devServer.path}/sockjs-node`,
    'webpack/hot/only-dev-server',
    "react", "react-router", "redux", "react-redux", "moment"
  ],
  bundle: [
    process.env.PLAYGROUND ? './test/playground.js' : './lib/main.js'
  ]
}
// -------- End Config -------------------


let compiler = webpack(config)
let server = new WebpackDevServer(compiler, config.devServer)

// ------ run the two servers -------
server.listen(8081, function() {
    console.log('Server is listening on port 8080')
})

// ------ Run the mock server --------
if (process.env.MOCK_SERVER) {
  require('./test/mock-server')
}

let express = require('express')
let httpProxy = require('http-proxy');
let proxy = httpProxy.createProxyServer({
  ws: true,
  target: 'http://0.0.0.0:8081'
})

let proxyServer = express()
proxyServer.use(compression())
proxyServer.use((req,res) => proxy.web(req, res))
proxyServer.listen(8080)
proxyServer.on('error', e => console.error(e))
