'use strict'

/**
 * Accepts as environment variables the following
 * @env NODE_ENV - Defines the node_env variable
 * @env COMPRESS - Uses minification
 * @env DEBUG - Enables debug mode on the application
 */

let webpack = require('webpack')
let path = require('path')
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

// A set of libraries that will be included but not parsed
let pathToMoment = path.resolve('node_modules/moment/min/moment-with-locales.min.js')
let pathToReact = path.resolve('node_modules/react/dist/react.min.js')
let pathToReactDom = path.resolve('node_modules/react-dom/dist/react-dom.min.js')
let pathToReactRedux = path.resolve('node_modules/react-redux/dist/react-redux.min.js')
let pathToReactMotion = path.resolve('node_modules/react-motion/build/react-motion.js')
let pathToRedux = path.resolve('node_modules/redux/dist/redux.min.js')

var ExtractTextPlugin = require("extract-text-webpack-plugin");

let plugins = [
  new CommonsChunkPlugin({
    name: 'vendor',
    filename: 'vendor.js'
  }),
  new ExtractTextPlugin("styles.css"),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    __DEV__: JSON.stringify(process.env.DEBUG),
    API_URL: JSON.stringify(process.env.API_URL),
    LOGIN_REDIRECT: JSON.stringify(process.env.LOGIN_REDIRECT)
  })
]

if (process.env.COMPRESS) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  )
}

let config = module.exports = {

  entry : {
    vendor: [
      "react", "react-router", "redux", "react-redux", "moment"
    ],
    bundle: ['./lib/main.js']
  },

  output: {
    path: './build',
    filename: 'bundle.js',
    publicPath: "lib/",
    chunkFilename: '[id].chunk.js',
  },

  setAlias(k, v) {
    this.resolve.alias[k] = v
  },

  externals: {},

  node: {
    dns: 'mock',
    net: 'mock'
  },

  module: {
    loaders: [
      { test: /\.(js|jsx)$/, loaders: ['babel?cacheDirectory'], exclude: /(node_modules|lib\/webcomponents)/ },
      { test: /\.ts(x?)$/, loaders: ['babel?cacheDirectory', 'ts-loader'], exclude: /node_modules/ },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style', ['css', 'postcss']), exclude: /node_modules/ },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css'), include: /node_modules/ },
      { test: /\.upcss$/, loader: ExtractTextPlugin.extract('style', ['css']), exclude: /node_modules/ },
      { test: /\.(woff(2)?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url?limit=100000' },
      { test: /\.(jpg|gif|png)$/, loader: 'url-loader' },
      { test: /\.json$/, loader: 'json-loader' }
    ],
    noParse: [

      pathToMoment,
      pathToReact,

      'jquery/dist/jquery.min.js',
      'semantic-ui/dist/semantic.min.js',
      'semantic-ui/dist/semantic.min.css'
    ]
  },

  resolve: {
    root: [path.resolve('node_modules'), path.resolve('src/lib'), path.resolve('src')],
    extensions: [
      '', '.js', '.jsx',
      '.ts', '.tsx',
      '.css',
      '.woff', '.woff2', '.ttf', '.eot', '.svg'
    ],
    alias: {}
  },

  plugins: plugins,

  devtool: process.env.COMPRESS ? null : process.env.DEBUG ? 'cheap-module-source-map' : 'cheap-module-eval-source-map',

  ts: {
    compiler: 'typescript'
  },

  postcss: function() {
    return {
      defaults: [

        // Needed for importing
        require('postcss-import')({
          onImport: function(files) {
            files.forEach(this.addDependency)
          }.bind(this)
        }),
        require('postcss-nested'),
        require('postcss-custom-properties')(),
        require('cssnano')(),
        require('rucksack-css')(),
        require('autoprefixer')({browsers: ['> 5%', 'IE 9', 'IE 11']})
      ]
    }
  }
}

// Setup aliases
config.setAlias('moment', pathToMoment)
config.setAlias('react', pathToReact)
config.setAlias('react-dom', pathToReactDom)
config.setAlias('react-redux', pathToReactRedux)
config.setAlias('react-motion', pathToReactMotion)
config.setAlias('redux', pathToRedux)
config.setAlias('joi', 'joi-browser')

if (process.env.REACT_LITE) {
  // Use a lightweight react version
  config.setAlias('react', 'react-lite')
  config.setAlias('react-dom', 'react-lite')
}
