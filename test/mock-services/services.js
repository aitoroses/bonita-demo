module.exports = [{
  method: 'GET',
  path: '/bonita/API/bpm/process', // s=Alta
  reply: function(params, query, body) {
    return [{
      id: 6761179961187631898
    }]
  },
  options: {
    requiresAuth: false
  },
},{
    method: 'GET',
    path: '/bonita/API/bpm/task', // f=processId=6761179961187631898
    reply: function(params, query, body) {
      return [
        {
          "displayDescription": "",
          "executedBy": "0",
          "rootContainerId": "1013",
          "assigned_date": "",
          "displayName": "Validar Presupuesto",
          "executedBySubstitute": "0",
          "dueDate": "2016-02-23 19:52:47.440",
          "description": "",
          "type": "USER_TASK",
          "priority": "normal",
          "actorId": "109",
          "processId": "6948445767255461542",
          "caseId": "1013",
          "name": "Validar Presupuesto",
          "reached_state_date": "2016-02-23 18:52:47.452",
          "rootCaseId": "1013",
          "id": "20051",
          "state": "ready",
          "parentCaseId": "1013",
          "last_update_date": "2016-02-23 18:52:47.452",
          "assigned_id": ""
        }
      ]
    },
    options: {
      requiresAuth: false
    }
},{
    method: 'GET',
    path: '/bonita/API/bpm/process/:processId/instantiation',
    reply: function(params, query, body) {
      this.res.end()
    },
    options: {
      requiresAuth: false
    }
}]
