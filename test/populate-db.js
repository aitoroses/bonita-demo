var mocker = require('mocker-data-generator')

var util = require('util')
var fs = require('fs')
var path = require('path')

// Models
var task = require('./mock-models/task')
var db = require('./mock-db')

function cleanObject(obj) {
  var clone = Object.assign({}, obj)
  Object.keys(clone).forEach(function(k) {
    if (k.match(/^_/)) {
      delete clone[k]
    }
  })
  return clone
}

var seedData = {
  task
}
var m = mocker(seedData)
m.generate('task', 40)
  .then(function(data) {
    var keys = Object.keys(data)
    for (var i = 0; i < keys.length; i++) {
      var k = keys[i]
      db[k].insert(data[k].map(cleanObject), function(err, newDocs) {
        if (err) {
          console.log(err)
          return console.log('Fail to insert ' + k + ' collection')
        }
      })
    }
    console.log('Database generated go play outside!!')
  })
