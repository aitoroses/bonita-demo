// Polyfill ES6
require('babel-polyfill')

// Global configuration
require('./config')

// Vendor Globals (not used by typescript)
require('./globals')

// JS Libs && CSS Libs
require('script!jquery/dist/jquery.min.js')
require('script!semantic-ui/dist/semantic.min.js')
require('semantic-ui/dist/semantic.min.css')

// Application entries
require('../assets/style/app.css')
require('../src')
