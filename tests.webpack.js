require('babel-polyfill')

var context = require.context('./src', true, /.*-test\.(js|jsx|ts|tsx)$/)
context.keys().forEach(context)
