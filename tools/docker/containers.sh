docker run -it -d --name wmock -p 8002:7003 -v $PWD:/data workspace \
  'npm run mock-server'

docker run -it -d --name wdev -e 'HMR_HOST=vagrant-vm' -e 'HMR_PORT=8001' -p 8001:8080 -v $PWD:/data workspace \
  'npm run dev'
