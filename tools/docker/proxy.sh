devd -p 8000 -A 0.0.0.0 \
  /app/=http://localhost:8001 \
  /api/=http://localhost:8002 \
  /static/=./static \
  /login/=./login \
  /Maxima/=http://localhost:9001 \
  /Maxima/_api/=http://localhost:9002 \
  /CNDForms/=http://localhost:5002 \
  /CNDForms/_api/=http://localhost:5001
