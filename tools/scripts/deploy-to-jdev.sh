RELEASE_DIR=$WORKSPACE_REPO/release
WORKSPACE_PROJECT=$EAPP_REPO/Workspace
PUBLIC_HTML_DIR=$WORKSPACE_PROJECT/Workspace/public_html
CURRENT_DIR=$PWD

echo "==> Starting release..."
cd $WORKSPACE_REPO
npm run release:china

if [ -d $RELEASE_DIR ]
  then
    # If EAPP repository it's defined
    if [ -d $EAPP_REPO ]
      then

        echo "==> Copying release files..."
        cp -r $RELEASE_DIR/* $PUBLIC_HTML_DIR

        # Navigate and commit
        echo "==> Versioning the changes..."

        cd $PUBLIC_HTML_DIR
        git add -A
        git commit -m "Update public_html"
        echo "==> Public HTML commited"

        cd $WORKSPACE_PROJECT
        git add -A
        git commit -m "Update public_html"
        echo "==> Workspace project commited"

        # Restore DIR
        cd $CURRENT_DIR
        unset CURRENT_DIR
        echo "==> Exiting..."
      else
        echo "==> There is no EAPP_REPO, verify env variable..."

    fi

  else
    echo "==> There is no release dir into WORKSPACE_REPO, verify WORKSPACE_REPO env variable..."
fi
